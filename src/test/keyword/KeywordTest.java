package keyword;

import com.codecool.pt.si3.pages.SimpleFormDemo;
import com.codecool.pt.si3.util.Driver;
import org.apache.poi.sl.usermodel.Sheet;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import waiter.Waiter;

import java.io.IOException;


public class KeywordTest {
    private static WebDriver driver;
    private static Driver driverUtil;
    private static WebDriverWait driverWait;

    private static  SimpleFormDemo pageSimpleFormDemo;
    private Waiter waiter = new Waiter();

    @BeforeAll
    public static void setup(){
        driverUtil = new Driver();
        driver = driverUtil.getWebDriver();
        pageSimpleFormDemo = new SimpleFormDemo(driver);
    }
    @BeforeEach
    public void goToPage(){
        pageSimpleFormDemo.goToPage();
    }
    @AfterAll
    public static void close() {
        driverWait = new WebDriverWait(driver ,10);
        driver.close();
    }

    @ParameterizedTest
    @MethodSource("dataProvider")
    public void singleFieldAndButton(String testcaseName,String keyword,String objectName,String objectType,String value) {
        pageSimpleFormDemo.setMessage("message");
        Assertions.assertEquals(pageSimpleFormDemo.getMessage(), "message");
    }

    public void executeTest (Sheet sheet) {

    }
    public Object[][] dataProvider() throws IOException {
        Object[][] object = null;
        return object;
    }
}
