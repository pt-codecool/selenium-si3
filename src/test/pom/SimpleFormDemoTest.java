import org.junit.jupiter.api.Test;

import com.codecool.pt.si3.pages.SimpleFormDemo;
import com.codecool.pt.si3.util.Driver;

import org.junit.jupiter.api.*;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;

import waiter.Waiter;

import java.util.Map;


public class SimpleFormDemoTest {
    private static WebDriver driver;
    private static Driver driverUtil;
    private static WebDriverWait driverWait;

    private static  SimpleFormDemo pageSimpleFormDemo;
    private Waiter waiter = new Waiter();

    @BeforeAll
    public static void setup(){
        driverUtil = new Driver();
        driver = driverUtil.getWebDriver();
        pageSimpleFormDemo = new SimpleFormDemo(driver);
    }
    @BeforeEach
    public void goToPage(){
        pageSimpleFormDemo.goToPage();
    }
    @AfterAll
    public static void close() {
        driverWait = new WebDriverWait(driver ,10);
        driver.close();
    }

    @ParameterizedTest
    @MethodSource("com.codecool.pt.si3.operation.DataProvider#exercise2")
    public void singleFieldAndButton(Map map) {
        String message = map.get("message").toString();
        pageSimpleFormDemo.setMessage(message);
        Assertions.assertEquals(pageSimpleFormDemo.getMessage(), message);
    }

}
