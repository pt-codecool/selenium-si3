package com.codecool.pt.si3.operation;

import com.codecool.pt.si3.util.ExcelReader;
import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class DataProvider {
    private static ExcelReader reader = new ExcelReader("./src/main/resources/");

    public DataProvider() {

    }
    public static Stream<Arguments> exercise2() {
        return reader.getStreamOfMapArray("data.xlsx",
                "Exercise2");
    }

}
